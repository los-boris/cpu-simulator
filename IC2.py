from time import sleep
from time import strftime
from datetime import datetime
from CU import CU
from Memory import ram
from Registers import registers
from Registers import memoryARegistry
from flask import Flask
from flask import render_template

app = Flask(__name__)

"""
def loader(self, codefile):
    print("Loading " + codefile + "...")
    self.R2 = self.MEMORY.loader(codefile)
    print("Processed: " + str(self.R2) + " lines.")
    self.running = True
    if self.running == True:
        print("The machine is running...")
    else:
        print("The machine is off")
"""

@app.route('/')
def circuitoIntegrado():
    # horaInicio = time.time()
    ejemplos = CU.ejemplos()
    PC = CU.programCounter()
    RAM = ram()

    # clock = time.time()
    HoraInicio = datetime.now()
    print("HORA INICIO")
    print(HoraInicio.strftime("%H:%M:%S")),

    sleep(1)
    print("\n")

    CIR = CU.instruccionregistro(ejemplos.instrucciones[PC.value])
    MAR = memoryARegistry(0)
    registroA = registers()
    registroB = registers()
    registroC = registers()
    registroD = registers()
    pause = CU.detener()
    salida = []

    def fetch():
        #       self.R3 = self.MEMORY.read(self.PC)
        #       print("||FETCH|| ==> address (" + strCIR.current) + ")")
        #       print(">>read(" + strCIR.opcode) + "==>" +CIR.FourBitsAddressInfo + ")")

        print('|| FETCH ||')
        print("\tLínea leída ==> %s" % CIR.current)
        print("\tOpcode encontrado ==> %s" % CIR.opcode)
        print("\tOperación ==> %s" % CIR.FourBitAdress)

        if len(CIR.FourBitAdress) < 4:
            if CIR.FourBitAdress != 'HALT' and (CIR.FourBitAdress).isdigit():
                MAR.addressBus = int(CIR.FourBitAdress)
            else:
                MAR.addressBus == 0000
        RAM.bus = RAM.dataBus(MAR.addressBus)
        print("\tOpcode encontrado ==> %s" % CIR.opcode)

        if CIR.decode() != 1111:
            print("\tInstruccion ==> %s" % CIR.decode())
            print("\tdirección MAR ==> %i" % MAR.addressBus)
            print("\tRAM data bus ==> %i" % RAM.bus)

    def decode():
        #      self.PC += 1
        #      try:
        #          self.IR = int(self.R3)
        #          self.R3 = "Push"
        #          print("\t||DECODE|| ==> decoding(" + str(self.IR) + ")")
        #          print("\tOperand(" + str(self.IR) + ")")
        #          print("Valid instruction")
        #      except ValueError:
        #          self.IR = self.R3
        #          print("\t||DECODE|| ==> decoding(" + self.IR + ")")
        #          print("Valid instruction")

        # R3 debe separarse en opcode y operadores
        # self.R3 = "Push" debe convertirse a la operación, por ejemplo ADD

        print('|| DECODE ||')

        if CIR.decode == 11 or CIR.decode == 111 or CIR.decode == 1001 or CIR.decode == 1010:
            print("\t=Utiliza dos bits por cada dirección=")
        else:
            if len(CIR.FourBitAdress) == 4:
                direccionBinaria = CIR.FourBitAdress
                MAR.addressBus = CIR.conversorbinario(direccionBinaria)

            if len(CIR.FourBitAdress) < 4:
                if CIR.FourBitAdress != 'HALT' and (CIR.FourBitAdress).isdigit():
                    MAR.addressBus = int(CIR.FourBitAdress)
                else:
                    MAR.addressBus == 0000
            RAM.bus = RAM.dataBus(MAR.addressBus)
            print("\tOpcode encontrado ==> %s" % CIR.opcode)

            if CIR.decode() != 1111:
                print("\tInstruccion ==> %s" % CIR.decode())
                print("\tdirección MAR ==> %i" % MAR.addressBus)
                print("\tRAM data bus ==> %i" % RAM.bus)

    def execute():
        #      op = self.R3
        #      if op == "Push":
        #          print("\t||EXECUTE|| ==> " + op)
        #          print("\tPushed(" + str(self.IR) + ")")
        #          self.MEMORY.push(self.IR)
        #      else:
        #          op1, op2 = self.MEMORY.pop()
        #          print("\t||EXECUTE|| ==> " + op + "(" + str(op1) + "," + str(op2) + ")")
        #          print("\tPop <==(" + str(op1) + ")")
        #          print("\tPop <==(" + str(op2) + ")")
        #          self.R1 = self.ALU.operate(op, op1, op2)        # Cambiar esto en ALU
        #          print("\tPush ==>(" + str(self.R1) + ")")
        #          self.MEMORY.push(self.R1)

        print("|| EXECUTE ||")
        decodificado = CIR.decode()
        operate = CU.operations()
        ALU = CU.ArithmeticLU()

        if decodificado == 0:
            registroD.storedValue = RAM.dataBus(int(CIR.FourBitAdress))
            salida.append(registroD.storedValue)
            print(f"\tRegistro guardado ==> {operate.output(registroB)}")

        if decodificado == 1:
            print(f"\tDirección de bus: {MAR.addressBus}")
            print(f"\tValor de la data en Bus: {RAM.bus}")
            operate.LD_A(registroA, RAM.bus)
            print(f"\tRegistro A: {registroA.storedValue}")

        if decodificado == 10:
            print(f"\tDirección de bus: {MAR.addressBus}")
            operate.LD_B(registroB, RAM.bus)
            print(f"\tValor de la data en Bus: {RAM.bus}")
            print(f"\tRegistro B: {registroB.storedValue}")

        if decodificado == 11:
            operandAND1 = CIR.FourBitAdress[2]
            operandAND2 = CIR.FourBitAdress[0]
            ALU.AND(operandAND1, operandAND2)
            print(f"\tApplying ALU")

        if decodificado == 100:
            operate.ILD_A(registroA, MAR.addressBus)
            print(f"\tDirección de bus: {MAR.addressBus}")
            print(f"\tRegistro A: {registroA.storedValue}")

        if decodificado == 101:
            operate.STR_A(RAM, MAR.addressBus, registroA)
            print(f"\tSe guardó Dato A en posición {MAR.addressBus}")
            print(f"\t")

        if decodificado == 110:
            operate.STR_B(RAM, MAR.addressBus, registroB)
            print(f"\tSe guardó Dato B en posición {MAR.addressBus}")

        if decodificado == 111:
            operandOR1 = CIR.FourBitAdress[2]
            operandOR2 = CIR.FourBitAdress[0]
            ALU.OR(operandOR1, operandOR2)
            print(f"\tApplying OR")

        if decodificado == 1000:
            operate.ILD_B(registroB, MAR.addressBus)
            print(f"\tDirección de bus: {MAR.addressBus}")
            print(f"\tRegistro B: {registroB.storedValue}")

        if decodificado == 1001:
            sumador1 = CIR.FourBitAdress[2]
            sumador2 = CIR.FourBitAdress[0]
            if sumador1 == "A":
                sumador1 = registroA
            if sumador1 == "B":
                sumador1 = registroB
            if sumador1 == "C":
                sumador1 = registroC
            if sumador1 == "D":
                sumador1 = registroD

            if sumador2 == "A":
                sumador2 = registroA
            if sumador2 == "B":
                sumador2 = registroB
            if sumador2 == "C":
                sumador2 = registroC
            if sumador2 == "D":
                sumador2 = registroD
            ALU.ADD(sumador1, sumador2)
            print(f"\tLa suma es {sumador2.storedValue}")

        if decodificado == 1010:
            restador1 = CIR.FourBitAdress[2]
            restador2 = CIR.FourBitAdress[0]

            if restador1 == "A":
                restador1 = registroA
            if restador1 == "B":
                restador1 = registroB
            if restador1 == "C":
                restador1 = registroC
            if restador1 == "D":
                restador1 = registroD

            if restador2 == "A":
                restador2 = registroA
            if restador2 == "B":
                restador2 = registroB
            if restador2 == "C":
                restador2 = registroC
            if restador2 == "D":
                restador2 = registroD

            ALU.SUB(restador1, restador2)
            print(f"\tLa resta es {restador2.storedValue}")

        if decodificado == 1011:
            saltarHacia = MAR.addressBus
            operate.JMPInstruccion(PC, saltarHacia)
            print("\tDirección actualizada")

        if decodificado == 1100:
            saltarHacia = MAR.addressBus
            operate.JMPInstruccionNegative(PC, saltarHacia)
            print("\tDirección actualizada")

        if decodificado == 1101:
            operandPUSH = CIR.FourBitAdress[2]
            ALU.PUSH(operandPUSH)
            print("\Pushed")

        if decodificado == 1110:
            operandPOP = CIR.FourBitAdress[2]
            ALU.POP(operandPOP)
            print("\tPoped")

        if decodificado == 1111:
            print(f"\t== FIN DE LAS INSTRUCCIONES ==")
            operate.HALT(pause)
        PC.update()
        print("")

    horafinal = datetime.now() - HoraInicio

    HoraInicio = CU.reloj()

    for i in range(0, (ejemplos.linea), 1):
        if PC.value <= ejemplos.linea and pause.det != 1:
            fetch()
            decode()
            if PC.value <= ejemplos.linea:
                execute()
                # print("Registro A > %i" % registroB.storedValue)
                # horafinal.ciclosActuales()
                print("\tDiferencia de tiempo : ", horafinal)
                print(HoraInicio.ciclosActuales)
                if pause.det!=1:
                    print("Resultados")
                    print("Registro A ==> %i" %registroA.storedValue)
                    print("Registro B ==> %i" %registroB.storedValue)
                    print("Registro C ==> %i" %registroC.storedValue)
                    print("Registro D ==> %i" %registroD.storedValue)
                    print("OPCODE ==> %s" %CIR.current)
                    print("RAM ==> %a" %RAM.data)
                    print("\n")

            if PC.value <= ejemplos.linea - 1:
                CIR = CU.instruccionregistro(ejemplos.instrucciones[PC.value])
            print("")
        else:
            i += 1
    return render_template('index.html',
                           aRegister = registroA.storedValue,
                           bRegister = registroB.storedValue,
                           cRegister = registroC.storedValue,
                           dRegister = registroD.storedValue,
                           pcRegister = PC.value,
                           iRegister = CIR.current,
                           sal = salida)