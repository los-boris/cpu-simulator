class ALU():

    """
        def __init__(self):
        self.zero = ""
        self.alu_ops = {
            'ADD': lambda a, b: round(a+b, 2),
            'SUB': lambda a, b: round(a-b, 2),
            'DIV': lambda a, b: round(b/a, 2),
            'MUL': lambda a, b: round(a*b, 2),
            'AND': lambda a, b: a & b,
            'OR': lambda a, b: a | b,
            'XOR': lambda a, b: a ^ b,
            'NOT': lambda a: ~a,
        }

    """


    #def zero(self, op1):
     #   if op1 == 0:
      #      return print("I can´t make an operation")

    def overflow(self):
        pass

    def negative(self, op1):
        if op1 < 0:
            print("I'm a negative number")

    def add(self, op1, op2):
        return round(op1 + op2, 2)

    def sub(self, op1, op2):
        return round(op1 - op2, 2)

    def div(self, op1, op2):
        return round(op1/op2, 2)

    def mul(self, op1, op2):
        return round(op1 * op2, 2)

    def And(self, op1, op2):
        return round(op1 & op2)

    def OR(self, op1, op2):
        return round(op1 | op2)

    def Xor(self, op1, op2):
        return round(op1 ^ op2)

    def Not(self, op1):
        return round(~op1)

    def comp(self, op1, op2):
        result = 1
        if op1 != op2:
            result = 0
        return result

    def operate(self, op, op1, op2):
        if op in self.alu_ops:
            return self.alu_ops[op](op1, op2)
        else:
            print("Error: operation not found")