class registers:
    usados = 0

    def __init__(self, linea = 0):
        if linea == 0:
            self.storedValue = linea
        else:
            self.storedValue = linea
            registers.usados += 1

    def clear(self):
        self.storedValue = 0
        registers.usados -= 1

    def vals(self):
        if self.storedValue > 0:
            return 1
        else: return 0

    def reset(self):
        self.storedValue = 0
        registers.usados = 0

class memoryARegistry:
    def __init__(self, PC):
        self.addressBus = PC
        self.nextDataAddressFetching = PC + 1