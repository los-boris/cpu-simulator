#Control is here!
from Memory import Memory
import time
from datetime import  datetime
import time
from collections import deque
STACK = deque([])

class CU:

    class reloj:
        ciclosOn =0
        
        def __init__(self):
            self.ciclos = self.ciclosOn
        def velocidad(self, timeStarted):
            return (self.ciclos/(datetime.now() - timeStarted))
        def ciclosActuales(self):
            self.ciclosOn += 1
            return self.ciclosOn

    class detener:
        valor = 0
        def __init__(self):
            self.det = self.valor

    class ejemplos:
        ejemplo = open("example03.code", 'r').read()
        leerEjemplo = ejemplo.split('\n')
        instructions = []
        for instruction in leerEjemplo:
            byte = instruction.upper()
            asterisco = byte[0]
            if(asterisco != '#'):
                instructions.append(byte)
        linea = len(instructions)

        def __init__(self):
            self.instrucciones = self.instructions

    class programCounter:
        valor = 0

        def __init__(self):
            self.value = self.valor

        def update(self, jmp = 1):
            self.value += jmp

    class instruccionregistro:
        def __init__(self, instructionPC):
            self.current = instructionPC
            index = instructionPC.find(' ')
            largoInstruccion = len(instructionPC)
            if index > 0:
                self.opcode = instructionPC[0:index]
                self.FourBitAdress = instructionPC[index+1:largoInstruccion]
                self.Primeros2bits = instructionPC[index+1: index+2]
                self.ultimos2bits = instructionPC[largoInstruccion-1:largoInstruccion]
            else:
                self.opcode = instructionPC # aqui vemos la direccion del halt a la que se va a ir
                self.FourBitAdress = instructionPC #
        def conversorbinario(self,BinaryAdress_string):
            binaryadress = ['0000',
                            '0001',
                            '0010',
                            '0111',
                            '0100',
                            '0101',
                            '0110',
                            '0111',
                            '1000',
                            '1001',
                            '1010',
                            '1011',
                            '1100',
                            '1101',
                            '1110',
                            '1111']
            binaryadresstoint = binaryadress.index(BinaryAdress_string)
            return binaryadresstoint
        def decode(self): #lee strings y binario
            opcode = self.opcode
            if opcode == '0000' or opcode == 'OUTPUT':
                return 0
            if opcode == '0001' or opcode == 'LD_A':
                return 1
            if opcode == '0010' or opcode == 'LD_B':
                return 10
            if opcode == '0011' or opcode == 'AND':
                return 11
            if opcode == '0100' or opcode == 'ILD_A':
                return 100
            if opcode == '0101' or opcode == 'STR_A':
                return 101
            if opcode == '0110' or opcode == 'STR_B':
                return 110
            if opcode == '0111' or opcode == 'OR':
                return 111
            if opcode == '1000' or opcode == 'ILD_B':
                return 1000
            if opcode == '1001' or opcode == 'ADD':
                return 1001
            if opcode == '1010' or opcode == 'SUB':
                return 1010
            if opcode == '1011' or opcode == 'JMP':
                return 1011
            if opcode == '1100' or opcode == 'JMP_N':
                return 1100
            if opcode == '1101' or opcode == 'PUSH':
                return 1101
            if opcode == '1110' or opcode == 'POP':
                return 1110
            if opcode == '1111' or opcode == 'HALT':
                return 1111

    class ArithmeticLU:
        def ADD(self, dato1, dato2):
            dato2.valorguardado = dato1.valorguardado + dato2.valorguardado
            return dato2
        def AND(self, dato1, dato2):
            return dato1 and dato2
        def OR(self, dato1, dato2):
            dato1 = dato1 or dato2
            return dato1
        def SUB(self, dato1, dato2):
            dato2.valorguardado = dato1.valorguardado - dato2.valorguardado
            return dato2
        def PUSH(self,dato1):
            dato1.valorguardado = STACK.append(dato1.valorguardado)

        def POP(self,dato1):
            dato1.valorguardado = STACK.pop(dato1.valorguardado)

    class operations:
        def output(self,B):
            return B.valorguardado

        def LD_A(self, datoA, data):
            datoA.valorguardado = data
            print("\tLoaded A")
            
        def LD_B(self, datoB, data):
            datoB.valorguardado = data
            print("\tLoaded B")
        
        def ILD_A(self, datoA, almacenarRegistroA):
            datoA.valorguardado = almacenarRegistroA
        
        def ILD_B(self, datoB, almacenarRegistroB):
            datoB.valorguardado = almacenarRegistroB

        def STR_A(self, ram, direccionRam, datoA):
            ram.data[direccionRam] = datoA.valorguardado
        
        def STR_B(self, ram, direccionRam, datoB):
            ram.data[direccionRam] = datoB.valorguardado

        def JMPInstruccion(self,objeto,jumpinstruccion):
            objeto.update(jumpinstruccion)

        def JMPInstruccionNegative(self,objeto,jumpinstruccion):
            objeto.update(jumpinstruccion)

        def HALT(self, pausar):
            pausar.interrupt = 1

        


"""

class InstructionRegister():
    def decode(self):
        opcode = self.decode()

        El estándar es: escribir el opcode (en 4 bits) y el mnemónico en mayúsculas

        if opcode == '0000' or opcode == 'OUTPUT':
            return 0
        if opcode == '0001' or opcode == 'LD_A':
            return 1
        if opcode == '0010' or opcode == 'LD_B':
            return 10
        if opcode == '0011' or opcode == 'AND':
            return 11
        if opcode == '0100' or opcode == 'ILD_A':
            return 100
        if opcode == '0101' or opcode == 'STR_A':
            return 101
        if opcode == '0110' or opcode == 'STR_B':
            return 110
        if opcode == '0111' or opcode == 'OR':
            return 111
        if opcode == '1000' or opcode == 'ILD_B':
            return 1000
        if opcode == '1001' or opcode == 'ADD':
            return 1001
        if opcode == '1010' or opcode == 'SUB':
            return 1010
        if opcode == '1011' or opcode == 'JMP':
            return 1011
        if opcode == '1100' or opcode == 'JMP_N':
            return 1100
        if opcode == '1101' or opcode == 'PUSH':
            return 1101
        if opcode == '1110' or opcode == 'POP':
            return 1110
        if opcode == '1111' or opcode == 'HALT':
            return 1111

"""
