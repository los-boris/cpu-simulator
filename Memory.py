#from collections import deque
from RAM import Espacios

class ram(Espacios):
    def __init__(self):
        addresses = []
        self.data = Espacios().datosMemoria
        self.capacidad = ram.bytesEncontrados
        for address in range(0, self.capacidad, 1):
            addresses.append(address)
        self.direccionActual = addresses
        self.bus = 0

    def dataBus(self, espacioRAM):
        return self.data[espacioRAM]

class Memory():

    def __init__(self):
        self.RAM = []
        self.STACK = deque([])
        self.SP = 0             # Puntero al inicio del stack

 #   def loader(self):
 #       pass
    def loader (self, codefile):
        try:
            for i in open(codefile, 'r+').read().splitlines():
                print(i)
                self.RAM.append(i)
            return len(self.RAM)

        except:
            print(codefile + " not found. Finishing process...")

    def read(self, addr):
        if addr < len(self.RAM):
            return self.RAM[addr]


    def push(self, data):
        self.STACK.appendleft(data)
        self.SP =self.STACK[0]

    def pop(self):
        op1 = self.STACK.popleft()
        op2 = self.STACK.popleft()
        return op1, op2
