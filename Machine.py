from IC import IC

class Machine:
    def __init__(self, codefile):
        self.c = IC()
        self.c.loader(codefile)
        self.c.run()