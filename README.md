# CPU-simulator

CPU Simulator es un software que simula un cpu de 4 bits. Este programa importa un archivo tipo ".code" para ejecutar las instrucciones . El programa puede recibir instrucciones ya sea como memónico o como binario y los guarda dentro de un regustro temporal para luego ser procesado línea por línea.

Este CPU cuenta con ciertas características:


## CU 

La función de la unidad de control es buscar instrucciones en la memoria principal, decodificarlas y luego ejecutarlas.

## ALU

Por sus siglas en ingles (Arithmetic Logic Unit). Gracias a esta parte del procesador es posible realizar una gran cantidad de operaciónes aritméticas básicas y lógicas.

## REGISTERS

Los registros son la manera más rápida que tiene el sistema de almacenar datos. Usualmente se miden por el número de bits que alacenan (En este caso es de 4 bits). Son una memoria de alta velocidad y poca capacidad.

## RAM

Por sus siglas en inglés (Random Acces Memory), es la encargada de mantener los datos de los programas y de guardar las instrucciones de estos que en cualquier momento la CPU puede pedir. Se denomina por 'acceso aleatorio' porque tiene la capacidad de leer y o de escribir en cualquier celda de memoria mediante un tiempo estipulado.

## CLOCK

El clock se refiere a un componente que regula la velocidad de las funciones de la computadora. Este chip vibra en cierta frecuencia cuando es aplicado, la velocidad de un procesador de una computadora se mide en clock speed, por ejemplo 1 Mhz es un millón de ciclos por segundo. 


## INSTRUCTIONS

En esta tabla se encuentran las instrucciones en lenguanje mnemonico y en binario.

En este [Link](https://drive.google.com/file/d/18hAofvMXULgUqUz8PMxtbSPkrwja3f1e/view?usp=sharing) encontrará las operaciónes que se realizan en el proyecto.

Los espacios en morado son funciones que nosotros agregamos. Agregamos la función POP y PUSH.


## Pre requisitos


Tener instalado [python](https://realpython.com/installing-python/) 3 en la computadora. Se recomienda tener una cuenta  [gitlab](https://gitlab.com/) para poder clonar el proyecto, aunque no es 100% necesario.

## Instalación

Dirigirte a la terminal de tu computadora e ingresar el siguiente comando :
```bash
git clone https://gitlab.com/los-boris/cpu-simulator.git
```
Este comando hará una copia local de todos los archivos del proyecto.

## USO

Para correr el programa debe de dirigirse a la terminal de su máquina y dirigirse hacia el directorio donde tiene guardado el proyecto de esta manera: 

```bash
C:\Users\your_User\cpu-simulator>
```
Una vez se encuentra en esta carpeta debe de correr el siguiente archivo de la siguiente manera:

```bash
python IC.py
```
## Support:
Si el proyecto no funciona como debe puede contactar a cualquiera de los siguientes correos: axocop@ufm.edu o boris.rendon@ufm.edu.
Este proyecto está alojado en gitlab.

## Autores

Creadores y desarrolladores del proyecto:

[Abner Xocop](https://gitlab.com/abnerxch) y
[Boris Rendón](https://github.com/BorisRendon) 

## Licencia
[MIT](https://choosealicense.com/licenses/mit/)

## Estado del proyecto:
Terminado
